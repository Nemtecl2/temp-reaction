/* eslint-disable @typescript-eslint/no-unused-vars */
/// <reference types="cypress" />

declare namespace Cypress {
    interface Chainable<Subject> {
        /**
         * Login to the app
         * @param {string} [email=Cypress.env('email')]       email
         * @param {string} [password=Cypress.env('password')] password
         * @example
         * cy.login()
         */
        login(email?: string, password?: string): Chainable<any>;

        /**
         * Check if the toastr message match
         * @param {string} [message] The message to check
         * @example
         * cy.checkToastMessage()
         */
        checkToastMessage(message: string): Chainable<any>;
    }
}
