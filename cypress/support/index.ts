/// <reference types="." />
// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

Cypress.Commands.add('login', (email = Cypress.env('email'), password = Cypress.env('password')) => {
    cy.visit('/');
    cy.get('input[name="email"]').type(email);
    cy.get('input[name="password"]').type(password);
    cy.get('button').contains('Se connecter').click();
    cy.get('span[data-test-id="helloworld"]').should('have.text', 'Bonjour !');
});

Cypress.Commands.add('checkToastMessage', (message) => {
    cy.get('.Toastify__toast').should('contain', message);
});
