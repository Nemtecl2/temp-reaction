const fs = require('fs');
const path = require('path');
const typescript = require('typescript');

function typescriptTransform(
    options = {
        tsOptions: {
            target: 'es2018',
        },
        extensions: ['.ts', '.tsx'],
    },
) {
    return function transform(file, enc, done) {
        const { base, ext } = path.parse(file.path);

        if (options.extensions.includes(ext) && !base.includes('.d.ts')) {
            const content = fs.readFileSync(file.path, enc);

            const { outputText } = typescript.transpileModule(content, {
                compilerOptions: options.tsOptions,
                fileName: path.basename(file.path),
            });

            this.parser.parseTransFromString(outputText);
            this.parser.parseFuncFromString(outputText);
        }

        done();
    };
}

module.exports = {
    input: [
        'src/**/*.{js,jsx,ts,tsx}',
        // Use ! to filter out files or directories
        '!src/**/*.spec.{js,jsx,ts,tsx}',
        '!src/i18n/**',
        '!**/node_modules/**',
    ],
    output: './',
    options: {
        sort: true,
        debug: true,
        func: {
            list: ['i18next.t', 'i18n.t', 't'],
            extensions: ['.js', '.jsx'],
        },
        trans: {
            component: 'Trans',
            i18nKey: 'i18nKey',
            defaultsKey: 'defaults',
            extensions: ['.js', '.jsx'],
            fallbackKey: function (ns, value) {
                return value;
            },
            acorn: {
                ecmaVersion: 10, // defaults to 10
                sourceType: 'module', // defaults to 'module'
                // Check out https://github.com/acornjs/acorn/tree/master/acorn#interface for additional options
            },
        },
        lngs: ['fr-FR', 'en'],
        // ns: [
        //     'locale',
        //     'resource'
        // ],
        defaultLng: 'fr-FR',
        defaultNs: 'translation',
        defaultValue: function (lng, ns, key) {
            return 'TO TRANSLATE';
        },
        resource: {
            loadPath: 'public/locales/{{lng}}/{{ns}}.json',
            savePath: 'public/locales/{{lng}}/{{ns}}.json',
            jsonIndent: 4,
            lineEnding: '\n',
        },
        nsSeparator: false, // namespace separator
        keySeparator: '.', // key separator
        interpolation: {
            prefix: '{{',
            suffix: '}}',
        },
    },
    transform: typescriptTransform({ extensions: ['.ts', '.tsx'] }),
};
