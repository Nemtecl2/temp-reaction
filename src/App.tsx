import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { AppProvider, CustomThemeProvider } from './core/contexts';
import { CircularProgress, CssBaseline } from '@material-ui/core';
import { Route, Router as ReactDomRouter, Switch } from 'react-router';
import { createBrowserHistory } from 'history';
import { routes } from './core/models';
import { Layout } from 'components';
import { Suspense } from 'react';
import MomentUtils from '@date-io/moment';
import 'core/utils/i18n';

function App() {
    const history = createBrowserHistory();
    return (
        <Suspense fallback={<CircularProgress size={100} thickness={1} disableShrink />}>
            <CustomThemeProvider>
                <AppProvider>
                    <CssBaseline />
                    <MuiPickersUtilsProvider utils={MomentUtils}>
                        <ReactDomRouter history={history}>
                            <Layout>
                                <Switch>
                                    {routes.map((r) => (
                                        <Route key={r.name} exact path={r.path} component={r.component} />
                                    ))}
                                </Switch>
                            </Layout>
                        </ReactDomRouter>
                    </MuiPickersUtilsProvider>
                </AppProvider>
            </CustomThemeProvider>
        </Suspense>
    );
}

export default App;
