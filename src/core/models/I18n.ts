import { i18n } from 'i18next';
import { TFunction } from 'react-i18next';

export interface I18n {
    t: TFunction<string>;
    i18n: i18n;
}
