import { Person, SupervisorAccount, Home as HomeIcon } from '@material-ui/icons';
import { NotConnectedHoc, ConnectedHoc, RoleHoc } from 'components';
import { Signin, Home, UserArea, AdminArea } from 'pages';
import { Role } from './Roles';

export interface Route {
    name: string;
    path: string;
    inSidebar: boolean;
    component: React.ComponentType;
    icon?: React.ElementType;
    requiredRole?: Role;
}

export const routes: Route[] = [
    {
        name: 'page.signin.label',
        path: '/sign-in',
        inSidebar: false,
        component: NotConnectedHoc(Signin),
    },
    {
        name: 'page.home.label',
        path: '/',
        inSidebar: true,
        component: ConnectedHoc(Home),
        icon: HomeIcon,
    },
    {
        name: 'page.userarea.label',
        path: '/user-area',
        inSidebar: true,
        component: ConnectedHoc(UserArea),
        icon: Person,
    },
    {
        name: 'page.adminarea.label',
        path: '/admin-area',
        inSidebar: true,
        requiredRole: Role.ADMIN,
        component: RoleHoc(AdminArea, Role.ADMIN),
        icon: SupervisorAccount,
    },
];
