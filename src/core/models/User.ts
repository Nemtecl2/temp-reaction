import { JwtPayload } from 'jwt-decode';
import { Role } from './Roles';

export interface User extends JwtPayload {
    firstname: string;
    lastname: string;
    roles: Role[];
}

export const defaultUser: User = {
    firstname: '',
    lastname: '',
    roles: [],
};
