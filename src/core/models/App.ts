import { User } from '.';

export interface App {
    appLoading: boolean;
    setAppLoading: Function;
    token: string;
    setToken: Function;
    user: User;
    setUser: Function;
}
