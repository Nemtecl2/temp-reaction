export interface Theme {
    currentTheme: string;
    setTheme: Function;
}
