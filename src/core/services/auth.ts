// import axios from 'axios';
interface AuthResponse {
    token: string;
}

export function authenticate(username: string, password: string): Promise<AuthResponse> {
    // Auth mock
    return Promise.resolve<AuthResponse>({
        token:
            username === 'admin' && password === 'admin'
                ? 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsYXN0bmFtZSI6IkFkbWluIiwiZmlyc3RuYW1lIjoiQWRtaW4iLCJyb2xlcyI6WyJBRE1JTiJdfQ.Wv7FCuQ_ab7j6OYX5vCSZlNkOPIwlEn37m3LaFwPVmA'
                : 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsYXN0bmFtZSI6IlRpdGkiLCJmaXJzdG5hbWUiOiJUb3RvIiwicm9sZXMiOlsiVVNFUiJdfQ.8RkJTTCyrQz2SYTCaT5OZe6r53-PrYxQw_1QrZ2c32w',
    });

    // To uncomment to join with your API
    // return axios.post<AuthResponse, AuthResponse>('/auth', { username, password });
}
