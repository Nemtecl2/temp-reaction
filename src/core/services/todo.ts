import axios from 'axios';
import { Todo } from 'core/models';

export function getTodos(): Promise<Todo[]> {
    return axios.get<Todo[], Todo[]>('/todos');
}
