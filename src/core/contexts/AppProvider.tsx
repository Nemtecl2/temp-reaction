import { createContext, useContext, useState } from 'react';
import { App, User } from 'core/models';
import jwt_decode from 'jwt-decode';
import { defaultUser } from 'core/models/User';

const AppContext = createContext<App>({
    appLoading: false,
    setAppLoading: () => {},
    token: '',
    setToken: () => {},
    user: defaultUser,
    setUser: () => {},
});

interface Props {
    children: React.ReactNode;
}
export function AppProvider({ children }: Props) {
    const t = localStorage.getItem('token') || '';
    const [token, setToken] = useState<string>(t);
    const [appLoading, setAppLoading] = useState<boolean>(false);
    const [user, setUser] = useState<User>(t ? jwt_decode<User>(t) : defaultUser);

    const persistToken = (token: string) => {
        // Automatically redirected to / or /login with the RouteContainer
        // / -> if logged
        // /login -> if not logged
        localStorage.setItem('token', token);
        setToken(token);
        setUser(token ? jwt_decode<User>(token) : defaultUser);
    };

    return (
        <AppContext.Provider
            value={{
                appLoading,
                setAppLoading,
                token,
                setToken: persistToken,
                user,
                setUser,
            }}
        >
            {children}
        </AppContext.Provider>
    );
}

export const useAppContext = () => useContext(AppContext);
