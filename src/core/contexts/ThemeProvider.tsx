import React, { createContext, useContext, useState } from 'react';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { Theme } from 'core/models';
import { getTheme } from 'core/themes';

const CustomThemeContext = createContext<Theme>({
    currentTheme: 'light',
    setTheme: null,
});

interface Props {
    children: React.ReactNode;
}
export function CustomThemeProvider({ children }: Props) {
    const currentTheme = localStorage.getItem('appTheme') || 'light';
    const [themeName, setThemeName] = useState(currentTheme);
    const theme = getTheme(themeName);

    const setTheme = (name: string) => {
        localStorage.setItem('appTheme', name);
        setThemeName(name);
    };

    const contextValue = {
        currentTheme: themeName,
        setTheme,
    };

    // See: https://stackoverflow.com/questions/64256794/react-cssbaseline-component-doesnt-update-after-material-ui-theme-changed
    return (
        <CustomThemeContext.Provider value={contextValue}>
            <MuiThemeProvider theme={{ ...theme }}>{children}</MuiThemeProvider>
        </CustomThemeContext.Provider>
    );
}

export const useThemeContext = () => useContext(CustomThemeContext);
