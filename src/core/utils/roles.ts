import { Role, User } from 'core/models';

export const isAdmin = (user: User): boolean => user.roles.includes(Role.ADMIN);

export const hasRole = (user: User, roleName: Role) => user.roles.includes(Role.ADMIN) || user.roles.includes(roleName);
