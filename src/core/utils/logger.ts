const PROD_LOGGER = {
    info: () => {},
    error: () => {},
    warn: () => {},
};
const logger = process.env.NODE_ENV !== 'production' ? console : PROD_LOGGER;

export default logger;
