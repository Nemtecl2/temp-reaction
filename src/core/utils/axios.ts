import axios from 'axios';
import { toast } from 'react-toastify';

export function setup() {
    axios.defaults.baseURL = process.env.REACT_APP_API_URL;
    axios.defaults.headers.common.Accept = 'application/json';

    axios.interceptors.request.use(async (r) => {
        const token = localStorage.getItem('token') || '';
        if (token) {
            r.headers.authorization = `Bearer ${token}`;
        }
        return r;
    });

    axios.interceptors.response.use(
        (r) => {
            if (r.status === 200) {
                return r.data;
            }
            return r;
        },
        (err) => {
            const r = err.response;
            if (r.status >= 400 && r.status < 500) {
                const msg = (r.data || {}).detail || 'Vérifiez les informations fournies';
                toast.error(msg);
            }

            if (r.status >= 500) {
                const msg = 'Erreur serveur';
                toast.error(msg);
            }

            return Promise.reject(err);
        },
    );
}

export default axios;
