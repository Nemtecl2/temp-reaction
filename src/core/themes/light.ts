import { createMuiTheme } from '@material-ui/core/styles';

export const light = createMuiTheme({
    palette: {
        primary: {
            main: '#001835',
            dark: '#00234c',
            light: '#334f6f',
            contrastText: '#fff',
        },
    },
});
