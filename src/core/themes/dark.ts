import { createMuiTheme } from '@material-ui/core/styles';

export const dark = createMuiTheme({
    palette: {
        type: 'dark',
        primary: {
            main: '#b23c17',
            dark: '#ff5722',
            light: '#ff784e',
            contrastText: '#fff',
        },
    },
});
