import { Theme } from '@material-ui/core';
import { light, dark } from './';

const themeMap: { [key: string]: Theme } = {
    dark,
    light,
};

export function getTheme(theme: string) {
    return themeMap[theme];
}
