import { light } from './light';
import { dark } from './dark';
import { getTheme } from './base';

export { light, dark, getTheme };
