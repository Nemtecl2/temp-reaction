import { Box, Divider, Drawer, IconButton, ListItem, ListItemIcon, ListItemText, makeStyles } from '@material-ui/core';
import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import MenuIcon from '@material-ui/icons/Menu';
import { I18n, Role, Route } from 'core/models';
import { withTranslation } from 'react-i18next';
import { ConnectedContainer, RoleContainer } from 'components/RouteContainer';
import { useHistory } from 'react-router';

type Props = I18n & {
    handleDrawer: any;
    open: Boolean;
    menus: Route[];
};

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    menuButton: {
        marginRight: 36,
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(9) + 1,
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 2),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
    },
}));

const Sidebar: React.FC<Props> = ({ handleDrawer, open, menus: inputMenus, t }) => {
    const history = useHistory();
    const classes = useStyles();
    const [menus, setMenus] = useState<Route[]>([]);

    useEffect(() => setMenus(inputMenus), [inputMenus]);

    const handleDrawerChange = () => {
        setMenus(menus.map((o) => ({ ...o, collapsed: false })));
        handleDrawer(!open);
    };

    const ListItemWrapper = ({ children, role }: { children: React.ReactNode; role?: Role }) =>
        role ? (
            <RoleContainer role={role}>{children}</RoleContainer>
        ) : (
            <ConnectedContainer>{children}</ConnectedContainer>
        );

    return (
        <Drawer
            variant="permanent"
            className={classnames(classes.drawer, open ? classes.drawerOpen : classes.drawerClose)}
            classes={{
                paper: classnames(open ? classes.drawerOpen : classes.drawerClose),
            }}
        >
            <Box className={classes.toolbar}>
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    onClick={handleDrawerChange}
                    edge="start"
                    className={classnames(classes.menuButton)}
                >
                    <MenuIcon />
                </IconButton>
            </Box>
            <Divider />
            {menus
                .filter((o) => o.inSidebar)
                .map(({ name, icon: Icon, requiredRole, path }) => (
                    <ListItemWrapper key={name} role={requiredRole}>
                        <ListItem button onClick={() => history.push(path)}>
                            <ListItemIcon>
                                <Icon />
                            </ListItemIcon>
                            <ListItemText primary={t(name)} />
                        </ListItem>
                    </ListItemWrapper>
                ))}
        </Drawer>
    );
};

export default withTranslation()(Sidebar);
