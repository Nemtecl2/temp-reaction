import React from 'react';
import { AppBar, Box, IconButton, makeStyles, Menu, MenuItem, Toolbar, Typography } from '@material-ui/core';
import classnames from 'classnames';
import { withTranslation } from 'react-i18next';
import { I18n } from 'core/models';
import { useThemeContext } from 'core/contexts/ThemeProvider';
import { ExitToApp, Brightness7, Brightness4 } from '@material-ui/icons';
import { useAppContext } from 'core/contexts';
import flagFR from 'assets/fr-FR.png';
import flagEN from 'assets/en.png';

type Props = I18n & {
    open: Boolean;
};

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        width: `calc(100% - 73px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    languageList: {
        padding: 0,
    },
    languageListItem: {
        padding: theme.spacing(1, 1.5),
    },
    flagSelected: {
        width: '1em',
    },
    flag: {
        width: 24,
    },
}));

const Header: React.FC<Props> = ({ open, i18n, t }) => {
    const classes = useStyles();
    const { setToken } = useAppContext();
    const { currentTheme, setTheme } = useThemeContext();
    const isDarkMode = Boolean(currentTheme === 'dark');
    const [languageAnchorEl, setLanguageAnchorEl] = React.useState<null | HTMLElement>(null);
    const languages = ['fr-FR', 'en'];
    const flags = {
        'fr-FR': flagFR,
        en: flagEN,
    };

    const openLanguageMenu = (event: React.MouseEvent<HTMLButtonElement>) => {
        setLanguageAnchorEl(event.currentTarget);
    };

    const closeLanguageMenu = () => {
        setLanguageAnchorEl(null);
    };

    const handleLanguageClick = (_event: React.MouseEvent<HTMLElement>, index: number) => {
        closeLanguageMenu();
        i18n.changeLanguage(languages[index]);
    };

    const changeTheme = () => {
        setTheme(isDarkMode ? 'light' : 'dark');
    };

    const logout = () => setToken('');

    return (
        <AppBar position="fixed" className={classnames(classes.appBar, open && classes.appBarShift)}>
            <Toolbar>
                <Typography variant="h6" noWrap>
                    {t('header.title.label')}
                </Typography>
                <Box ml="auto">
                    <IconButton color="inherit" onClick={openLanguageMenu}>
                        <img src={flags[i18n.language]} alt={i18n.language} className={classes.flagSelected} />
                    </IconButton>
                    <Menu
                        classes={{ list: classes.languageList }}
                        anchorEl={languageAnchorEl}
                        keepMounted
                        open={Boolean(languageAnchorEl)}
                        onClose={closeLanguageMenu}
                    >
                        {languages.map(
                            (language, index) =>
                                i18n.language !== language && (
                                    <MenuItem
                                        classes={{ root: classes.languageListItem }}
                                        key={language}
                                        onClick={(event) => handleLanguageClick(event, index)}
                                    >
                                        <img src={flags[language]} alt={language} className={classes.flag} />
                                    </MenuItem>
                                ),
                        )}
                    </Menu>
                    <IconButton color="inherit" onClick={changeTheme}>
                        {!isDarkMode ? <Brightness4 /> : <Brightness7 />}
                    </IconButton>
                    <IconButton color="inherit" onClick={logout}>
                        <ExitToApp />
                    </IconButton>
                </Box>
            </Toolbar>
        </AppBar>
    );
};

export default withTranslation()(Header);
