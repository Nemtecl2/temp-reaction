import { Box, makeStyles } from '@material-ui/core';
import { useAppContext } from 'core/contexts';
import AppLoader from '../AppLoaders';
import { useState } from 'react';
import Header from 'components/Header';
import Sidebar from 'components/Sidebar';
import { routes } from 'core/models';

type Props = {
    children: React.ReactNode;
};

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(1, 2),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
}));

const Layout: React.FC<Props> = ({ children }) => {
    const { token } = useAppContext();

    const classes = useStyles();
    const [open, setOpen] = useState(false);

    const handleDrawer = (newOpen) => {
        setOpen(newOpen);
    };

    return (
        <>
            {token ? (
                <>
                    <Box className={classes.root}>
                        <Header open={open} />
                        <Sidebar handleDrawer={handleDrawer} open={open} menus={routes} />
                        <Box component="main" className={classes.content}>
                            <Box className={classes.toolbar} />
                            <Box>{children}</Box>
                        </Box>
                    </Box>
                </>
            ) : (
                <Box>{children}</Box>
            )}
            <AppLoader />
        </>
    );
};

export default Layout;
