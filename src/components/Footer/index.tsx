import { Box, makeStyles } from '@material-ui/core';

// TODO: à vérifier
const useStyles = makeStyles((theme) => ({
    footer: {
        height: 50,
        padding: `0 ${theme.spacing(4)}`,
    },
    footerContent: {
        height: '100%',
    },
}));

const Footer = () => {
    const styles = useStyles();
    return (
        <Box className={styles.footer}>
            <Box display="flex" justifyContent="center" alignItems="center" className={styles.footerContent}>
                Reaction ©2021
            </Box>
        </Box>
    );
};

export default Footer;
