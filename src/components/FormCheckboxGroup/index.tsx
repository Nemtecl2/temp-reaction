import {
    FormControl,
    FormControlLabel,
    FormControlLabelProps,
    FormHelperText,
    FormLabel,
    Radio,
    RadioGroup,
    RadioGroupProps,
} from '@material-ui/core';

type Props = RadioGroupProps & {
    label?: string;
    error: boolean;
    helperText: string;
    errorMessage: string;
    items: FormControlLabelProps[];
};

const FormCheckboxGroup: React.FC<Props> = ({
    items,
    name,
    onChange,
    label,
    error,
    errorMessage,
    helperText,
    ...otherRgProps
}) => {
    return (
        <FormControl component="fieldset" error={error}>
            {label && <FormLabel component="legend">{label}</FormLabel>}
            <RadioGroup aria-label={name} name={name} value={name} onChange={onChange} {...otherRgProps}>
                {items.map(({ value, label: radioLabel, ...otherFcProps }, i) => (
                    <FormControlLabel
                        value={value}
                        control={<Radio />}
                        label={radioLabel}
                        key={`radio-button-option-${i}`}
                        {...otherFcProps}
                    />
                ))}
            </RadioGroup>
            {(error || helperText) && <FormHelperText>{errorMessage || '' + helperText}</FormHelperText>}
        </FormControl>
    );
};

export default FormCheckboxGroup;
