import { Route, Switch } from 'react-router';
import { routes } from 'core/models';

const Router = () => {
    return (
        <Switch>
            {routes.map((r) => (
                <Route key={r.name} exact path={r.path} component={r.component} />
            ))}
        </Switch>
    );
};

export default Router;
