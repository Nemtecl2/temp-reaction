import { Backdrop, CircularProgress } from '@material-ui/core';
import { useAppContext } from 'core/contexts';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

// React StrictMode generate warning when using current makeStyles version
// It appears only in development mode
// See : https://stackoverflow.com/questions/61220424/material-ui-drawer-finddomnode-is-deprecated-in-strictmode
// Warning : removing React.StrictMode tag is not a good practice !
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        backdrop: {
            zIndex: theme.zIndex.drawer + 1,
        },
    }),
);

const AppLoader = () => {
    const classes = useStyles();
    const { appLoading } = useAppContext();
    return (
        appLoading && (
            <Backdrop className={classes.backdrop} open={true}>
                <CircularProgress size={100} thickness={1} disableShrink />
            </Backdrop>
        )
    );
};

export default AppLoader;
