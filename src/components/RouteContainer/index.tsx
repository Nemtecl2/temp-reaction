import React, { ReactElement } from 'react';
import { Redirect } from 'react-router';
import { useAppContext } from 'core/contexts';
import { hasRole } from 'core/utils/roles';
import { Role } from 'core/models';

const DEFAULT_NOT_CONNECTED_ROUTE = '/sign-in';
const DEFAULT_CONNECTED_ROUTE = '/';

/**
 * Redirect the user to the default connected route when the user
 * has not the role and intend to access protected-connected route
 */
export function RoleContainer({ children, role }: { children: React.ReactNode; role: Role }) {
    const { user } = useAppContext();
    return hasRole(user, role) ? (children as ReactElement) : <Redirect to={DEFAULT_CONNECTED_ROUTE} />;
}

/**
 * Display children if the user is connected
 */
export function ConnectedContainer({ children }: { children: React.ReactNode }) {
    const { token } = useAppContext();
    return token && (children as ReactElement);
}

/**
 * Display children if the user is not connected
 */
export function NotConnectedContainer({ children }: { children: React.ReactNode }) {
    const { token } = useAppContext();

    return !token && (children as ReactElement);
}

/**
 * Display children if the user has the role
 */
export function RoleRedirect({ children, role }: { children: React.ReactNode; role: Role }) {
    const { user } = useAppContext();
    return hasRole(user, role) && (children as ReactElement);
}

/**
 * Redirect the user to the default not-connected route when the user
 * is not-connected and intend to access connected route
 */
export function ConnectedRedirect({ children }: { children: React.ReactNode }) {
    const { token } = useAppContext();
    return token ? (children as ReactElement) : <Redirect to={DEFAULT_NOT_CONNECTED_ROUTE} />;
}

/**
 * Redirect the user to the default connected route when the user is
 * connected and intend to access not connected route
 */
export function NotConnectedRedirect({ children }: { children: React.ReactNode }) {
    const { token } = useAppContext();

    return token ? <Redirect to={DEFAULT_CONNECTED_ROUTE} /> : (children as ReactElement);
}

export function RoleHoc<Props>(Component: React.ComponentType<Props>, role: Role): React.ComponentType<Props> {
    return ({ ...props }) => {
        return (
            <RoleRedirect role={role}>
                <Component {...props} />
            </RoleRedirect>
        );
    };
}

export function ConnectedHoc<Props>(Component: React.ComponentType<Props>): React.ComponentType<Props> {
    return ({ ...props }) => {
        return (
            <ConnectedRedirect>
                <Component {...props} />
            </ConnectedRedirect>
        );
    };
}

export function NotConnectedHoc<Props>(Component: React.ComponentType<Props>): React.ComponentType<Props> {
    return ({ ...props }) => {
        return (
            <NotConnectedRedirect>
                <Component {...props} />
            </NotConnectedRedirect>
        );
    };
}
