import { SyntheticEvent, useEffect, useState } from 'react';
import { Avatar, Button, Container, makeStyles, TextField, Typography } from '@material-ui/core';
import { LockOutlined } from '@material-ui/icons';
import { auth } from 'core/services';
import { useAppContext } from 'core/contexts';
import { I18n, User } from 'core/models';
import jwt_decode from 'jwt-decode';
import { withTranslation } from 'react-i18next';

const { authenticate } = auth;

const useStyles = makeStyles((theme) => ({
    paper: {
        paddingTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

type Props = I18n & {};
interface Form {
    username: string;
    password: string;
}
const Signin: React.FC<Props> = ({ t }) => {
    const { setToken, setAppLoading, setUser } = useAppContext();
    const classes = useStyles();
    const [form, setForm] = useState<Form>({ username: '', password: '' });

    useEffect(() => setAppLoading(false), [setAppLoading]);

    const setField = (name: 'username' | 'password', value: string) => setForm({ ...form, [name]: value });

    const handleSubmit = (e: SyntheticEvent) => {
        e.preventDefault();
        setAppLoading(true);
        authenticate(form.username, form.password)
            .then(({ token }: { token: string }) => {
                setToken(token);
                setUser(jwt_decode<User>(token));
                setAppLoading(false);
            })
            .finally(() => setAppLoading(false));
        // We set app loading to false in the layout when iframe is loaded
        // must be uncommented after removing iframe
        // and remove setAppLoading in catch
    };

    return (
        <Container component="main" maxWidth="xs">
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlined />
                </Avatar>
                <Typography component="h1" variant="h5">
                    {t('page.signin.label')}
                </Typography>
                <form className={classes.form} onSubmit={handleSubmit}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="username"
                        label={t('page.signin.username.label')}
                        name="username"
                        autoComplete="username"
                        value={form.username}
                        onChange={(e) => setField('username', e.target.value)}
                        autoFocus
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label={t('page.signin.password.label')}
                        type="password"
                        id="password"
                        value={form.password}
                        onChange={(e) => setField('password', e.target.value)}
                        autoComplete="current-password"
                    />
                    <Button fullWidth type="submit" variant="contained" color="primary" className={classes.submit}>
                        Se connecter
                    </Button>
                </form>
            </div>
        </Container>
    );
};

export default withTranslation()(Signin);
