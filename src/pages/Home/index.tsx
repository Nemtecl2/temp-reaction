import { Box, Grid } from '@material-ui/core';
import { DataGrid, GridCellParams, GridColDef } from '@material-ui/data-grid';
import { I18n } from 'core/models';
import { withTranslation } from 'react-i18next';
import { todo } from 'core/services';
import { useEffect, useState } from 'react';
import { Check, Close } from '@material-ui/icons';
import { red, green } from '@material-ui/core/colors';
import { DatePicker, DateTimePicker, TimePicker } from '@material-ui/pickers';
import moment, { Moment } from 'moment';

type Props = I18n & {};

const Home: React.FC<Props> = ({ t }) => {
    const [loading, setLoading] = useState(false);
    const [todos, setTodos] = useState([]);
    const [date, setDate] = useState<Moment>(moment());

    const columns: GridColDef[] = [
        { field: 'id', headerName: t('home.todos.id'), flex: 1 },
        { field: 'userId', headerName: t('home.todos.user.id'), flex: 1 },
        { field: 'title', headerName: t('home.todos.title.label'), flex: 1 },
        {
            field: 'completed',
            headerName: t('home.todos.completed.label'),
            renderCell: (params: GridCellParams) =>
                params.value ? <Check style={{ color: green[500] }} /> : <Close style={{ color: red[500] }} />,
            flex: 1,
        },
    ];

    const getTodos = () => {
        setLoading(true);
        todo.getTodos()
            .then((data) => {
                setTodos(data);
            })
            .finally(() => setLoading(false));
    };

    useEffect(() => {
        getTodos();
    }, []);

    return (
        <>
            <Box component="span" data-test-id="helloworld">
                {t('page.home.label')}
            </Box>
            <Grid container direction="column" justify="center" alignItems="center">
                <Grid item>
                    <DatePicker value={date} onChange={setDate} />
                </Grid>
                <Grid item>
                    <TimePicker value={date} onChange={setDate} />
                </Grid>
                <Grid item>
                    <DateTimePicker value={date} onChange={setDate} />
                </Grid>
                <DataGrid rows={todos} columns={columns} loading={loading} pagination />
            </Grid>
        </>
    );
};

export default withTranslation()(Home);
