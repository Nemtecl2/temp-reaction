import { Box } from '@material-ui/core';
import { I18n } from 'core/models';
import React from 'react';
import { withTranslation } from 'react-i18next';

type Props = I18n & {};

const AdminArea: React.FC<Props> = ({ t }) => {
    return <Box>{t('page.adminarea.label')}</Box>;
};

export default withTranslation()(AdminArea);
