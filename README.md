Todo
--> Composants (greenbig / CD27 --> shuffle.dev/editor)
_ Data autocomplete
_ Radio
\_ Checkbox
\_ DatePicker
\_ Select / Dropdown
--> Formulaires (avec verification etc yup material ui formik)
--> Storybook (langue / theme)
--> Docsify/Readme (Architecture, getting started, tests, création composants, override material uin, override theme, creation route, configuration, I18n, useContext vs store, détailler chaque composants, index.ts, fcomp, import absolu, expliquer utilisation material UI pour spacing et layout, https://www.attineos.com/blog/tech/imports-absolus-de-modules-pour-react-et-react-native, formulaire, formik, translations, suspense, listing de toutes les technos, passage sur chaque fichier pour voir si oublie de chose, seulement le dossier public, container role/not connected/etc, parser, utiliser seulement public https://create-react-app.dev/docs/using-the-public-folder)
--> // TODO
--> Layout (translate the whole page, styling)
--> Changement de langue
--> Maj version
--> Suspense
--> Docsify on github (https://opensource.com/article/20/7/docsify-github-pages)
--> Proxy
--> Cypress
--> mettre fichier + image dans public plutot que dans un dossier assets
--> Discuter avec Julien sur le dossier assets vs public
--> https://create-react-app.dev/docs/setting-up-your-editor/
--> Tester sur tous les navigateurs

# Reaction

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). It contains libraries and a file structure allowing to create a front-end application more quickly.

Its architecture was inspired by the following links :

-   https://www.sitepoint.com/react-with-typescript-best-practices/
-   https://dev.to/farazamiruddin/an-opinionated-guide-to-react-folder-structure-file-naming-1l7i

## Libraries

### Core

-   [TypeScript](https://www.typescriptlang.org/)
-   [React](https://github.com/facebook/react) & [React DOM](https://github.com/facebook/react) for building user interfaces.
-   [React Router](https://github.com/reactjs/react-router) to handle in-app routing.
-   [Material UI](https://material-ui.com/)

### Utilities

-   [axios](https://github.com/axios/axios) for HTTP Requests.
-   [React i18n](https://react.i18next.com/) for translations
-   [React Toastify](https://github.com/fkhadra/react-toastify) for notifications
-   [classnames](https://github.com/JedWatson/classnames)
-   [Husky](https://github.com/typicode/husky/tree/master) for attaching to git hooks and run scripts.

### Tests

-   [Jest](https://create-react-app.dev/docs/running-tests/) for unit tests.
-   [cypress.io](https://docs.cypress.io/guides/overview/why-cypress.html) for e2e tests.

### Build system

-   CRA build system that uses [Webpack](https://github.com/webpack/webpack) and [Babel](https://github.com/babel/babel)

### Developer Experience

-   [tslint](https://github.com/palantir/tslint) for linting TypeScript files.
-   [VS Code Snippets](https://code.visualstudio.com/docs/editor/userdefinedsnippets)
-   [Webpack Dev Server](https://webpack.js.org/configuration/dev-server/) as live development server.

Checkout [this link](https://github.com/facebook/create-react-app#whats-included) for more informations

## Directory structure

```bash
.
├── .vscode                        # VSCode configuration folder.
│   ├── component.code-snippets    # React Snippets.
│   ├── favicon.ico                # IDE extensions recommendations.
│   └── settings.ico               # IDE settings.
├── build/                         # Built, ready to serve app.
├── cypress/                       # e2e tests folder.
├── public                         # Root folder for configurations.
│   ├── favicon.ico                # Favicon.
│   ├── index.html                 # html file.
│   └── manifest.json              # Manifest.
├── node_modules                   # Node Packages.
├── src                            # Source code.
│   ├── components                 # React Components.
│   │   ├── Layout                 # Example of component folder structure.
│   │   │   └── index.tsx          # TypeScript XML code.
│   │   └── index.ts               # Exports all elements in this folder.
│   ├── core                       # Set of shared content across the app.
│   │   ├── contexts               # React contexts files.
│   │   │   ├── AppContext.tsx     # Application context.
│   │   │   └── index.ts           # Exports all elements in this folder.
│   │   ├── models                 # Shared TypeScript models.
│   │   │   ├── menu.model.ts      # Example
│   │   │   └── index.ts           # Exports all elements in this folder.
│   │   └── services               # Services for consuming an API.
│   │   │   ├── users.ts           # Service example.
│   │   │   └── index.ts           # Exports all elements in this folder.
│   │   └── utils                  # Utils files.
│   │       ├── axios.ts           # Axios configuration.
│   │       └── index.ts           # Exports all elements in this folder.
│   ├── pages                      # React Components that represents a screen.
│   │   ├── Home                   # Example of pages folder structure.
│   │   │   └── index.tsx          # TypeScript XML code.
│   │   └── index.ts               # Exports all elements in this folder.
│   └── index.tsx                  # Entry point.
├── .eslintrc.js                   # Lint configuration.
├── .gitignore                     # Tells git which files to ignore.
├── .prettierrc.js                 # Prettier configuration.
├── cypress.json                   # Cypress configuration.
├── package.json                   # Package configuration.
├── README.md                      # This file.
└── tsconfig.json                  # TypeScript transpiler configuration.
```

## Usage

```bash

# Install dependencies
$ yarn install

# Running
$ yarn start # This starts the app in development mode

# Building
$ yarn build # This builds the app in production mode

# Tests
$ yarn test

# Install cypress
$ npx cypress install --force

# Tests e2e
$ yarn cypress:open # Automatically runs all tests
$ yarn cypress:run  # Automatically runs all tests
```

## Authentication and Authorization

HOC, bearer, methodes roles...

## Environments

There are differents environments files.

Here is a summary :

| File                     | Environment                  | Command           |
| ------------------------ | ---------------------------- | ----------------- |
| `.env`                   | Local                        | `yarn start`      |
| `.env.development`       | Development (develop branch) | `yarn build`      |
| `.env.development.local` | Development (in local)       | `yarn start:dev`  |
| `.env.release`           | Staging (release branch)     | `yarn build:rel`  |
| `.env.releése.local`     | Staging (in local)           | `yarn start:rel`  |
| `.env.production`        | Production (master branch)   | `yarn build:prod` |
| `.env.production.local`  | Production (in local)        | `yarn start:prod` |

⚠️ `.env.*.local` files are ignored by git. To use them, copy their similar. For example, you can create `.env.int.local` from `.env.int`

## Developments

### Absolute imports

### Base components

--> Material UI
--> Material UI picker
--> Material UI DataGrid
--> AutoComplete
--> Override des composants

### Create a component / page

Following the architecture, you have to create a folder with the name of your component/page.
This folder will contain the following files :

-   index.tsx

In the index.tsx file, you can use the code snippet "fcomp" to generate a component skeleton.

???? A DEV

### Styling

### Translations

dans le code I18n & pour l'interface

### Suspense

### Forms

### Layout and spacing

https://material-ui.com/components/grid/

### Create a story

## CI/CD

Section code quality pour husky / eslint / prettier ... ?

gitlabci, husky...

https://create-react-app.dev/docs/setting-up-your-editor/

https://docs.cypress.io/guides/continuous-integration/gitlab-ci.html#Testing-in-Chrome-and-Firefox-with-Cypress-Docker-Images

## Tests

### Unit tests

### End-to-end tests

## FAQ

### Why TypesScript ?

TypeScript is the superset of javascript. The code is faster to implement, easier to understand and to refactor. Also, there are fewers bugs. Finally, it helps the developer in having the correct workflow.

### Custom Webpack vs CRA

This project uses the CRA configuration rather than its own webpack. Indeed, the CRA configuration is sufficient for most projects because it covers many cases : css preprocessor, testing, development server, custom environments, optimized build with chunks...

However, if you need configuration not covered by CRA, you can use the [ `eject` command](## `npm-run-eject` -or- `yarn-eject` )

### Axios, Typescript and interceptors

By default, the response is an `AxiosResponse<T>` type. If the request is successful, you can directly access the data this way :

```javascript
axios.interceptors.response.use(
    (r) => r.data,
    ....
);
```

Nevertheless, axios requests still expect `AxiosResponse<T>` .
It's possible to change the type this way :

```javascript
// not using interceptor
axios.get < string > ('/users').then((r: AxiosReponse<string> ) => console.log(r.data))

// using interceptor
axios.get < string, string > .then((data: string) => console.log(data));

// signature example
AxiosInstance.post <T = any, R = AxiosResponse<T>> (url: string, data ? : any, config ? : AxiosRequestConfig | undefined): Promise <R>
```

See :

-   https://github.com/axios/axios/issues/1510
-   https://github.com/axios/axios/pull/1605/files

### Class VS Functional Component

Functional component are much easier to read and test because they are plain JavaScript functions without state or lifecycle-hooks. You end up with less code. They help you to use best practices. It will get easier to separate container and presentational components because you need to think more about your component’s state if you don’t have access to setState() in your component. The React team [mentioned](https://reactjs.org/blog/2015/10/07/react-v0.14.html#stateless-functional-components) that there may be a performance boost for functional component in future React versions

## `npm run eject` or `yarn eject`

**Note: this is a one-way operation. Once you `eject` , you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject` . The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
